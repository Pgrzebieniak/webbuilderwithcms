﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBuilder.Data.Repository
{
     interface IRepository<T>
    {
        void Add(T entity);
        bool Remove(int entityId);
       // bool Remove(T entity);
        T Get(int entityId);
        bool Update(T entity);
        IEnumerable<T> GetAll();
    }
}
