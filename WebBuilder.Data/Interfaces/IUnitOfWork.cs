﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Repositories;

namespace WebBuilder.Data.Interfaces
{
    interface IUnitOfWork
    {
        ProjectRepository ProjectRepository { get; }
        FilesRepository FilesRepository { get; }
        UserRepository UserRepository { get; }
        TemplateRepository TemplateRepository { get; }
        void Save();
    }
}
