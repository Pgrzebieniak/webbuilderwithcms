﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Repository;
using WebBuilder.Models;

namespace WebBuilder.Data.Interfaces
{
    interface IUserRepository : IRepository<User>
    {
        User GetUserById(string userId);
         User Get(int userId);
        IEnumerable<User> GetAll();
    }
}
