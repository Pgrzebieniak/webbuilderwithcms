﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Repository;
using WebBuilder.Models;

namespace WebBuilder.Data.Interfaces
{
    interface IProjectRepository : IRepository<Project>
    {
        int GetProjectId(string projectName);
        IEnumerable<Project> GetAllUserProjects(string userId);
        Project GetProjectByFilePath(string path);
    }
}
