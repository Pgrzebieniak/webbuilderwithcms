﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Repository;
using WebBuilder.Model.Models;

namespace WebBuilder.Data.Interfaces
{
    interface ITemplateRepository : IRepository<Template>
    {
        int GetTemplateByPath(string path);
    }
}
