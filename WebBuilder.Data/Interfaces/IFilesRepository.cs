﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Repository;
using WebBuilder.Models;

namespace WebBuilder.Data.Interfaces
{
    interface IFilesRepository : IRepository<Files>
    {
    }
}
