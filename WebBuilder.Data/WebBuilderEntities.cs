﻿using MySql.Data.Entity;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using WebBuilder.Model.Models;
using WebBuilder.Models;

namespace WebBuilder.Data
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
   public class WebBuilderEntities : IdentityDbContext<User>
    {
        public WebBuilderEntities() : base("MySqlContext")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

       // public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Files> Files { get; set; }
        public DbSet<Template> Templates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
