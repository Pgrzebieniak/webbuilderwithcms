﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Interfaces;
using WebBuilder.Models;

namespace WebBuilder.Data.Repositories
{
    public class FilesRepository : IFilesRepository
    {
        private readonly WebBuilderEntities _webBuilderEntities;

        public FilesRepository(WebBuilderEntities webBuilderEntities)
        {
            _webBuilderEntities = webBuilderEntities;
        }
        public void Add(Files files)
        {
            _webBuilderEntities.Files.Add(files);
        }

        public Files Get(int entityId)
        {
            return _webBuilderEntities.Files
                 .Where(x => x.ProjectId == entityId)
                 .Select(x => x).SingleOrDefault();
        }

        public IEnumerable<Files> GetAll()
        {
            return _webBuilderEntities.Files.ToList();
        }

        public bool Remove(Files entity)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int filesId)
        {
            try
            {
                var file =
                    _webBuilderEntities.Files.Find(filesId);
                _webBuilderEntities.Files.Remove(file);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Files files)
        {
            try
            {
                _webBuilderEntities.Entry(files).State =
                    EntityState.Modified;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
