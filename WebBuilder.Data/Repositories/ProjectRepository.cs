﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Interfaces;
using WebBuilder.Models;

namespace WebBuilder.Data.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly WebBuilderEntities _webBuilderEntities;

        public ProjectRepository(WebBuilderEntities webBuilderEntities)
        {
            _webBuilderEntities = webBuilderEntities;
        }
        public void Add(Project project)
        {
            _webBuilderEntities.Projects.Add(project);
        }

        public Project Get(int projectId)
        {
            return _webBuilderEntities.Projects
                .Where(x => x.ProjectId == projectId)
                .Select(x => x).SingleOrDefault();
        }

        public int GetProjectId(string projectName)
        {
            return _webBuilderEntities.Projects
                .Where(x => x.ProjectName == projectName)
                .Select(x => x.ProjectId).SingleOrDefault();
        }

        public IEnumerable<Project> GetAll()
        {
            return _webBuilderEntities.Projects.ToList();
        }

        public IEnumerable<Project> GetAllUserProjects(string userId)
        {
            return _webBuilderEntities.Projects
                .Where(x => x.UserId == userId).Select(x => x);

        }

        public Project GetProjectByFilePath(string path)
        {
            var projectId =
                _webBuilderEntities.Files
                .Where(x => x.MainFilePath == path)
                .Select(x => x.ProjectId).FirstOrDefault();

            return _webBuilderEntities.Projects
                .Where(x => x.ProjectId == projectId)
                .Select(x => x).SingleOrDefault();
        }

        public bool Remove(int projectId)
        {
            try
            {
                var project =
                    _webBuilderEntities.Projects.Find(projectId);
                _webBuilderEntities.Projects.Remove(project);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Project project)
        {
            try
            {
                _webBuilderEntities.Entry(project).State =
                    EntityState.Modified;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
