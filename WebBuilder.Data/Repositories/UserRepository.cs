﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Interfaces;
using WebBuilder.Models;

namespace WebBuilder.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly WebBuilderEntities _webBuilderEntities;

        public UserRepository(WebBuilderEntities webBuilderEntities)
        {
            _webBuilderEntities = webBuilderEntities;
        }

        public void Add(User user)
        {
            _webBuilderEntities.Users.Add(user);
        }

        public User GetUserById(string userId)
        {
            return _webBuilderEntities.Users.Where(x => x.Id == userId).Select(x => x).FirstOrDefault();
        }
        public User Get(int userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll()
        {
            throw new NotImplementedException();
        }

        public bool Remove(User entity)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int entityId)
        {
            throw new NotImplementedException();
        }

        public bool Update(User entity)
        {
            throw new NotImplementedException();
        }
    }
}
