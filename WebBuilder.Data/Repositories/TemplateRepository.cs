﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Interfaces;
using WebBuilder.Model.Models;

namespace WebBuilder.Data.Repositories
{
    public class TemplateRepository : ITemplateRepository
    {
        private readonly WebBuilderEntities _webBuilderEntities;

        public TemplateRepository(WebBuilderEntities webBuilderEntities)
        {
            _webBuilderEntities = webBuilderEntities;
        }
        public void Add(Template template)
        {
            _webBuilderEntities.Templates.Add(template);
        }

        public Template Get(int entityId)
        {
            return _webBuilderEntities.Templates
                .Where(x => x.Id == entityId)
                .Select(x => x).SingleOrDefault();
        }

        public int GetTemplateByPath(string path)
        {
            return _webBuilderEntities.Templates
                .Where(x => x.MainFilePath == path)
                .Select(x => x.Id).SingleOrDefault();
        }

        public IEnumerable<Template> GetAll()
        {
            return _webBuilderEntities.Templates.Select(x => x);
        }

        public bool Remove(Template entity)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int templatetId)
        {
            try
            {
                var template =
                    _webBuilderEntities.Templates
                    .Find(templatetId);
                _webBuilderEntities.Templates
                    .Remove(template);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Template template)
        {
            try
            {
                _webBuilderEntities.Entry(template).State =
                    EntityState.Modified;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
