﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Data.Interfaces;
using WebBuilder.Data.Repositories;

namespace WebBuilder.Data
{
  public class UnitOfWork : IUnitOfWork
    {
        private FilesRepository _filesRepository;
        private ProjectRepository _projectRepository;
        private UserRepository _userRepository;
        private TemplateRepository _templateRepository;
        private readonly WebBuilderEntities _webBuilderEntities=new WebBuilderEntities();
        
        public UnitOfWork(WebBuilderEntities webBuilderEntities)
        {
            _webBuilderEntities = webBuilderEntities;
        }

        public UnitOfWork()
        {
        }

        public ProjectRepository ProjectRepository
        {
            get
            {

                if (_projectRepository == null)
                {
                    _projectRepository = new ProjectRepository(_webBuilderEntities);
                }
                return _projectRepository;
            }
        }

        public FilesRepository FilesRepository
        {
            get
            {

                if (_filesRepository == null)
                {
                    _filesRepository= new FilesRepository(_webBuilderEntities);
                }
                return _filesRepository;
            }
        }

        public UserRepository UserRepository
        {
            get
            {

                if (_userRepository == null)
                {
                    _userRepository = 
                        new UserRepository(_webBuilderEntities);
                }
                return _userRepository;
            }
        }

        public TemplateRepository TemplateRepository
        {
            get
            {

                if (_templateRepository == null)
                {
                    _templateRepository = new TemplateRepository(_webBuilderEntities);
                }
                return _templateRepository;
            }
        }

        public void Save()
        {
            _webBuilderEntities.SaveChanges();
        }

    }
}
