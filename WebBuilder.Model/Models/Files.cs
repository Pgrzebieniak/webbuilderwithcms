﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebBuilder.Models
{
    public class Files
    {
        [Key]
        public int ProjectFilesId { get; set; }
        public string ProjectPath { get; set; }
        public string MainFilePath { get; set; }
        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }
    }
}