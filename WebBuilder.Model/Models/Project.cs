﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using WebBuilder.Model.Models;

namespace WebBuilder.Models
{
    public class Project
    {
        [Key]
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }

        
        [ForeignKey("User")]
        public string UserId { get; set; }
        [ForeignKey("Template")]
        public int TemplateId { get; set; }
        public virtual User User { get; set; }
        public virtual Template Template { get; set; }
    }
}