﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBuilder.Models;

namespace WebBuilder.Model.Models
{
    public class Template
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string FolderTemplatePath { get; set; }
        public string MainFilePath { get; set; }
        public string MainPicturePath { get; set; }

        //public virtual ICollection<Project> projects { get; set; }
    }
}
