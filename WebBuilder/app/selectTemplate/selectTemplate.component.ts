﻿import { Component, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from "@angular/http";
//import {AuthenticationService} from '../authentication/authentication.service';
import { BuilderService } from "../builder.service";
import { Template } from "../model/Template";
//import Authenticationservice = require("../authentication/authentication.service");
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';//bez tego dostaje unsafe value used in a resource URL context
import { AuthenticationService } from '../authentication.service';
//import { AlertService} from '../alert.service';

@Component({
    selector: 'my-selectTemplate',
    templateUrl: "app/selectTemplate/selectTemplate.component.html",
    styleUrls: ["app/selectTemplate/selectTemplate.component.css"]

    //providers: [AuthenticationService]
})
export class TemplatesComponent {
    private templates: Template[];
    private mainFilePath: string;
    private imgSrc:string;
    private selectedItemSrc: SafeResourceUrl=null;
    private showHide: boolean = false;

    constructor(private router: Router,
        private builderService: BuilderService,
        private elementRef: ElementRef, private sanitizer: DomSanitizer, private service: AuthenticationService) {
    }

    getTemplates() {
        this.builderService.getTemplates()
            .subscribe(
            (templates: Template[]) => {
                this.templates = templates;
            });
    }

    showPage(imgSrc: string) {
        this.imgSrc=imgSrc;
        this.selectedItemSrc = this.sanitizer.bypassSecurityTrustResourceUrl(imgSrc);
        //this.appearGallery(false);
    }

    // appearGallery(visible: any) {
    //     this.carouselVisible = visible !== false;
    // }

    closeSelectedTemplate() {
        this.selectedItemSrc = null;
        //this.appearGallery(true);
    }

    selectTemplate() {
        debugger;
        this.builderService
            .selectTemplate(this.imgSrc)
            .subscribe((res) => {
                if (res != null) {
                    let link = ['/design', res];
                    this.router.navigate(link);
                }
            },
            error => {
                alert(error.text());
                console.log(error.text());
            });
    }
    //           .subscribe(res => {
    //               if (res.status === 200) {
    //                  console.log("ok");
    //                  let link = ['/design', this.imgSrc];
    //                 this.router.navigate(link);
    //               }
    //               else {
    //                   console.log(res.json());
    //               }
    //           });
    //   }//'design/:path'

    ngOnInit() {

        //this.loggedUser= this.service.checkCredentials();
        this.getTemplates();
        // var userName=localStorage.getItem('userName');
        // if (userName === null) {
        // }
        // else {
        //     this.loggedUser=userName;
        // }


        // let x = document.createElement("script");
        // x.setAttribute("type", "text/javascript");
        // x.setAttribute("src", "app/selectTemplate/carousel3D.js");
        // document.body.appendChild(x);

        //var s = document.createElement("script");
        //s.type = "text/javascript";
        //s.src = "app/selectTemplate.component/carousel3D.js";
        //this.elementRef.nativeElement.appendChild(s);

    }

    // isEmptyObject(obj: string) {
    //     debugger;
    //     if (obj === null) {
    //         return true;
    //     }
    //     else {
    //         return false;
    //     }
    // }

}




