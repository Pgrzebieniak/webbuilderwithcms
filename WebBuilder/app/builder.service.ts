﻿import { Files } from './model/Files';
import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs/Rx';
import { Headers, RequestOptions, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';
import { Template } from './model/Template';
import { Project } from './model/Project';
import { ViewModel } from './viewModel';

declare var $: any;

@Injectable()
export class BuilderService {
    private baseUrl = "http://localhost:10551/"
    private loginUrl = this.baseUrl + "api/account/Login";
    private logOutUrl = this.baseUrl + "api/account/LogOut";
    private registerUrl = this.baseUrl + "api/account/Register";
    private templatesUrl = this.baseUrl + "api/template/GetAllTemplates";
    private selectedTemplateUrl = this.baseUrl + "api/template/CreateTemplateForUser";
    private userProject = this.baseUrl + "api/project/GetAllUserProject";
    private projectFiles = this.baseUrl + "api/files/GetProjectFiles";
    private saveChangesUrl = this.baseUrl + "api/files/SaveChanges";
    private uploadImgUrl = this.baseUrl + "api/files/Upload";
    private donwloadProjectUrl = this.baseUrl + "api/project/DownloadProject";
    private projectDetailsUrl = this.baseUrl + "api/project/ProjectDetails"
    private changeProjectNameUrl = this.baseUrl + "api/project/ChangeProjectName";
    private removeProjectUrl = this.baseUrl + "api/project/DeleteProject/";
    private sourceHtmlOfFileUrl = this.baseUrl + "api/files/sourceFileHtml";
    private saveEditedHtmlUrl=this.baseUrl + "api/files/saveChangesInEditetSource";

    constructor(
        private http: Http) {
    }

    login(email: string, password: string) {
        let body = JSON.stringify({ email, password });
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.loginUrl, body, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    logOut() {
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.logOutUrl, null, options)
            .map(res => res)
            .catch(this.handleError);
    }

    register(email: string, name: string, lastName: string,
        confirmPassword: string, password: string) {
        let body = JSON.stringify({
            email, name, lastName,
            confirmPassword, password
        });
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.registerUrl, body, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    getProjectFiles(project: Project) {
        let body = JSON.stringify(project);
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.projectFiles, body, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    getTemplates() {
        return this.http.get(this.templatesUrl)
            .map(res => res.json());
    }

    getUserProjects() {
        return this.http.get(this.userProject)
            .map(res => res.json())
            .catch(this.handleError);
    }

    selectTemplate(selectedItemSrc: string) {
        let body = JSON.stringify({ selectedItemSrc });
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.selectedTemplateUrl, body, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    projectDetails(mainFilePath: string) {
        let body = JSON.stringify({ mainFilePath });
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.projectDetailsUrl, body, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    changeProjectName(project: Project) {
        let body = JSON.stringify(project);
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.changeProjectNameUrl, body, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    sourceHtmlOfFile(mainFilePath: string) {
        let body = JSON.stringify({ mainFilePath });
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.sourceHtmlOfFileUrl, body, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    removeProject(id: string) {
        return this.http.delete(this.removeProjectUrl + id)
            .map(res => res)
            .catch(this.handleError);
    }

    upload(fileToUpload: any, path: string, oldImgName: string) {
        let body = new FormData();
        let headers = new Headers({ "Content-Type": "application/json" });
        body.append("file", fileToUpload);
        body.append("path", path);
        body.append("oldImg", oldImgName);
        return this.http.post(this.uploadImgUrl, body)
            .map(res => res.json())
            .catch(this.handleError);
    }

    saveChanges(viewModel: ViewModel) {
        let body = JSON.stringify(viewModel);
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.saveChangesUrl, body, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    saveChangesInEditedSource(mainFilePath:string, htmlEditedSource:string){
         let body = JSON.stringify({mainFilePath,htmlEditedSource});
        let headers = new Headers({ "Content-Type": "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.saveEditedHtmlUrl, body, options)
            .map(res => res)
            .catch(this.handleError);
    }

    downloadProject(mainFilePath: string) {
        let body = ({ mainFilePath });
        return this.http.post(this.donwloadProjectUrl, body,
            { responseType: ResponseContentType.Blob }).map(
            (res) => {
                return new Blob([res.blob()], { type: 'application/zip' })
            })
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};

    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `$ {error.status} - $ {error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}


