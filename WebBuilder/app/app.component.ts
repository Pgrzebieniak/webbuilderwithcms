﻿import { Component } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';
import { BuilderService } from './builder.service';
import { Project } from "./model/Project";
import { Files } from "./model/Files";
import { AuthenticationService } from './authentication.service'

@Component({
    selector: 'my-app',
    templateUrl: 'app/component.html',
    styleUrls: ['app/componentCss/component.css', 'app/componentCss/style.css']
})
export class AppComponent {
    private loggedUser: string;
    private email: string; private name: string; private lastName: string;
    private password: string; private confirmPassword: string;
    private projects: Project[];
    private files: Files;

    constructor(private route: ActivatedRoute, private _router: Router, private builderService: BuilderService,
        private service: AuthenticationService) { }

    showHideLoginBox(element: string) {
        // if (element === "login") {
        var display = document.getElementById(element).style.display;
        if (display === "none") {
            document.getElementById(element).style.display = "block";

        }
        else {
            document.getElementById(element).style.display = "none";
        }
    }
    showHideMyProject() {
        debugger;
        // item.className.replace(" animation-examples one","");
        var item = document.getElementById('dropdown').style.display;
        if (item === "none") {
            document.getElementById('dropdown').style.display = "block";
        }
        else {
            document.getElementById('dropdown').style.display = "none";
        }
    }

    login() {
        this.builderService
            .login(this.email, this.password)
            .subscribe((
                response) => {
                this.loggedUser = response;
                if (this.loggedUser != null) {
                    debugger;
                    localStorage.setItem('userName', this.loggedUser);
                }
                // this.router.navigate(['home']);
            },
            error => {
                alert(error.text());
                console.log(error.text());
            }
            );
    }

    register() {
        this.builderService
            .register(this.email, this.name, this.lastName, this.confirmPassword, this.password)
            .subscribe((
                response) => {
                this.loggedUser = response;
                if (this.loggedUser != null) {
                    localStorage.setItem('userName', this.loggedUser);
                }
                // this.router.navigate(['home']);
            },
            error => {
                alert(error.text());
                console.log(error.text());
            }
            );
    }

    logOut() {
        debugger;
        this.builderService
            .logOut()
            .subscribe(res => {
                if (res.status === 200) {
                    localStorage.removeItem('userName');
                    this.loggedUser = this.service.checkCredentials();
                }
                else {
                    console.log(res.json());
                }
            });
    }

    getProjectFiles(project: Project) {
        this.builderService.getProjectFiles(project)
            //.selectTemplate(this.imgSrc)
            .subscribe(
            (files: Files) => {
                this.files = files;
                debugger;
                let link = ['/design', files.MainFilePath];
                this._router.navigate(link);
            });
    }

    getUserProjects() {
        this.builderService.getUserProjects()
            .subscribe(
            (projects: Project[]) => {
                this.projects = projects;
            });
    }


    removeProject(project: Project) {
        debugger;
        this.builderService
            .removeProject(project.ProjectId.toString())
            .subscribe(res => {
                if (res.status === 200) {
                    let index = this.projects.findIndex(x => x.ProjectId === project.ProjectId);
                    this.projects.splice(index, 1);
                }
                else {
                    console.log(res.json());
                }
            });
    }

    ngOnInit() {
        debugger;
        this.loggedUser = this.service.checkCredentials();
        this.getUserProjects();
    }
    // ngOnInit() {
    //  this.service.checkCredentials();
    // }
}