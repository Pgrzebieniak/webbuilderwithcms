﻿export class ViewModel {

    constructor(element: string, currentText: string, newText: string, color: string, fontSize:string,
        marginLeft: string, marginRight: string, marginTop: string, marginBottom: string,path:string) {

        this.Element = element; this.CurrentText = currentText; this.NewText = newText;
        this.Color = color; this.FontSize=fontSize; this.MarginLeft = marginLeft; this.MarginRight = marginRight;
         this.MarginTop = marginTop; this.MarginBottom = marginBottom;this.Path=path;
    }


    Element: string;
    CurrentText: string;
    NewText: string;
    Color: string;
    FontSize:string;
    MarginLeft: string;
    MarginRight: string;
    MarginTop: string;
    MarginBottom: string;
    Path:string;
}