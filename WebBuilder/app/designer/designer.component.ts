﻿import { AfterViewInit, Component, ElementRef, ViewChild, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { ActivatedRoute, Params } from '@angular/router';
import { ColorPickerService } from 'angular2-color-picker';
import { ViewModel } from "../viewModel";
import { BuilderService } from '../builder.service';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';//bez tego dostaje unsafe value used in a resource URL context
import { Response } from '@angular/http';
import { BrowserXhr } from '@angular/http';
import { Project } from "../model/Project";

declare var $: any;
declare var componentHandler: any;

@Component({
    selector: 'my-designer',
    // host: {
    //     '(click)': 'handleClick($event)',
    //  },
    templateUrl: 'app/designer/designer.component.html',
    styleUrls: ['app/designer/design.css']
})

export class DesignerComponent implements AfterViewInit {
    private itemColor: string;
    private viewModel: ViewModel;
    private imgResolution: string; private imgWidth: string; private imgHeight: string;
    private elementName: string;
    private selectedImg: HTMLImageElement;
    private htmlCollection: Array<HTMLElement>;
    private currentCollection: Array<HTMLElement> = new Array<HTMLElement>();
    private selectedItem: string;
    private currentValueSelectedItem: string;
    private selectedHtmlElement: string;
    private fontSize: string;
    private selectedItemSrc: SafeResourceUrl = null;
    private mainFilePath: string;
    private marginTop: string; private marginBottom: string; private marginLeft: string; private marginRight: string;
    private sub: any;
    private project: Project;
    private projectName: string; htmlSource: string;
    private value: string;

    constructor(private router: Router, private sanitizer: DomSanitizer, private zone: NgZone,
        private elementRef: ElementRef, private cpService: ColorPickerService,
        private builderService: BuilderService, private route: ActivatedRoute) {
    }

    @ViewChild("designedPage") designedPage: ElementRef;
    @ViewChild("container") container: ElementRef;
    @ViewChild("fileInput") fileInput: ElementRef;

    hiddenShowToolsBox() {
        var className = document.getElementById("closeOpenAngle").className;
        if (className == "fa fa-angle-left") {
            document.getElementById("closeOpenAngle").className = "fa fa-angle-right";
            document.getElementById("selectTheme").style.marginLeft = "-210px";
        }
        else {
            document.getElementById("closeOpenAngle").className = "fa fa-angle-left";
            document.getElementById("selectTheme").style.marginLeft = "0px";
        }
    }

    openTab(tabName: any, id: any) {
        let tabcontent: any;
        this.clearSelectedElement();
        debugger;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (var i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";

        }
        let tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = 'tablinks';
        }
        document.getElementById(tabName).style.display = "block";
        document.getElementById(id).className = 'tablinks active';
    }

    changeTheme(className: any) {

        var color1 = window.getComputedStyle(
            document.querySelector('.' + className),
            ':before')
            .getPropertyValue('background-color');

        var color2 = window.getComputedStyle(
            document.querySelector('.' + className),
            '')
            .getPropertyValue('background-color');

        var x = document.getElementsByTagName("object");
        x[0].contentDocument.getElementById("header").style.backgroundColor = color2;
    }

    getAllPictureFromPage() {
        debugger;
        if (this.selectedImg) {
            var element = document.getElementById("allPicture");
            element.style.display = "block";
            this.selectedImg = null;
            this.imgResolution = null;
        }
        else {
            this.htmlCollection = this.designedPage.nativeElement.contentDocument.all;

            if (this.currentCollection.length == 1) {
                this.removeImages();
            }
            this.currentCollection = [];

            for (var i = 0; i < this.htmlCollection.length; i++) {
                if (this.htmlCollection[i].localName === "img") {
                    this.currentCollection.push(this.htmlCollection[i]);
                }
            }
        }
    }

    selectedPicture(img: any) {
        debugger;
        this.removeImages();
        this.selectedImg = img;
        //this.imgResolution = img.naturalWidth + " " + "x" + " " + img.naturalHeight + " " + "px";
        this.imgWidth = img.naturalWidth + "px";
        this.imgHeight = img.naturalHeight + "px";
    }

    resizeWidthImg(sign: string) {
        let value = parseInt(this.imgWidth.replace(/[^0-9]/g, ''));
        if (sign === "+") {
            value++;
        }
        else {
            value--;
        }
        this.imgWidth = value + "px";
        var foundItem = this.currentCollection.find(x => x === this.selectedImg);
        foundItem.style.width = this.imgWidth;
    }

    resizeHeightImg(sign: string, ) {
        let value = parseInt(this.imgHeight.replace(/[^0-9]/g, ''));
        if (sign === "+") {
            value++;
        }
        else {
            value--;
        }
        this.imgHeight = value + "px";
        var foundItem = this.currentCollection.find(x => x === this.selectedImg);
        foundItem.style.width = this.imgHeight;
        //  let filepath:string;
        // var txtFile = new File(filepath);

    }

    removeImages() {
        debugger;
        var element = document.getElementById('allPicture');
        element.style.display = "none";
    }

    getSelectedItem(selected: string) {
        debugger;
        this.zone.run(() => {// update view
            this.clearSelectedElement();
            this.currentValueSelectedItem = selected;
            this.selectedItem = selected;
            var foundItem = this.currentCollection.find(x => x.innerHTML === selected);
            this.fontSize = window.getComputedStyle(foundItem).getPropertyValue("font-size");
            this.itemColor = window.getComputedStyle(foundItem).getPropertyValue("color");
            this.marginTop = window.getComputedStyle(foundItem).getPropertyValue("margin-top");
            this.marginBottom = window.getComputedStyle(foundItem).getPropertyValue("margin-bottom");
            this.marginLeft = window.getComputedStyle(foundItem).getPropertyValue("margin-left");
            this.marginRight = window.getComputedStyle(foundItem).getPropertyValue("margin-right");
            foundItem.className += " animation-examples one";
            debugger;
        });
    }

    clearSelectedElement() {
        debugger;
        if (this.selectedItem != null) {
            var item = this.currentCollection.find(x => x.innerHTML === this.currentValueSelectedItem);
            if (item) {
                if (item.className.includes(" animation-examples one") && item) {
                    item.className = item.className.replace(" animation-examples one", "");
                }
            }
        }
    }

    changeFontSize(sign: any) {
        debugger;
        let size: number = parseInt(this.fontSize.replace(/[^0-9]/g, ''));
        if (sign === "+") {
            size++;
        }
        else {
            size--;
        }
        this.fontSize = size.toString() + "px";
        var foundItem = this.currentCollection.find(x => x.innerHTML === this.selectedItem);
        foundItem.style.fontSize = this.fontSize;
    }

    changeMargin(option: any, margin: string, sign: string) {
        debugger;
        let value = parseInt(margin.replace(/[^0-9]/g, ''));
        if (sign === "+") {
            value++;
        }
        else {
            value--;
        }
        var foundItem = this.currentCollection.find(x => x.innerHTML === this.selectedItem);

        switch (option) {
            case "marginTop":
                this.marginTop = value + "px";
                foundItem.style.marginTop = this.marginTop;
                break;
            case "marginLeft":
                this.marginLeft = value + "px";
                foundItem.style.marginLeft = this.marginLeft;
                break;
            case "marginRight":
                this.marginRight = value + "px";
                foundItem.style.marginRight = this.marginRight;
                break;
            case "marginBottom":
                this.marginBottom = value + "px";
                foundItem.style.marginBottom = this.marginBottom;
                break;
        }
    }

    getHtmlElements(selectedElement: any) {
        this.elementName = selectedElement;
        this.clearSelectedElement();
        this.selectedItem = null;
        this.currentValueSelectedItem = null;
        this.htmlCollection = this.designedPage.nativeElement.contentDocument.all;
        this.currentCollection = [];

        for (var i = 0; i < this.htmlCollection.length; i++) {

            if (this.htmlCollection[i].localName === selectedElement) {
                this.currentCollection.push(this.htmlCollection[i]);
            }
        }
    }


    saveChanges() {
        debugger;
        var item = this.currentCollection.find(x => x.innerHTML === this.selectedItem);
        item.innerHTML = this.currentValueSelectedItem;
        item.style.color = this.itemColor;
        //this.selectedItem = this.currentValueSelectedItem;
        let url: string = this.designedPage.nativeElement.contentDocument.URL;

        this.viewModel = new ViewModel(this.elementName, this.selectedItem, this.currentValueSelectedItem, this.itemColor,
            this.fontSize, this.marginLeft, this.marginRight, this.marginTop, this.marginBottom, url);

        this.builderService
            .saveChanges(this.viewModel)
            .subscribe(res => {
                if (res.status === 200) {

                }
                else {
                    console.log(res.json());
                }
            });
    }

    uploadImage() {
         let url: string = this.designedPage.nativeElement.contentDocument.URL;
        let fi = this.fileInput.nativeElement;
        if (fi.files && fi.files[0]) {
            let fileToUpload = fi.files[0];
            this.builderService
                .upload(fileToUpload, url, this.selectedImg.outerHTML)
                .subscribe(res => {
                    debugger;
                    //   var img=  this.currentCollection.find(x=>x.getAttribute("src")==this.selectedImg.src);
                    //  img=res;
                    this.selectedImg.src = res;
                });
        }
    }

    downloadProject() {
        document.getElementById("loader").style.display = "block";
        this.builderService.downloadProject(this.mainFilePath).subscribe(res => {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(res);
            a.href = url;
            a.download = "MyWebSite.zip";
            document.body.appendChild(a);
            a.click();
            document.getElementById("loader").style.display = "none";
        });
    }

    getProjectDetails() {
        this.builderService
            .projectDetails(this.mainFilePath)
            .subscribe(
            (project: Project) => {
                debugger;
                this.project = project;
                this.projectName = project.ProjectName;
            });
    }

    changeProjectName() {
        this.project.ProjectName = this.projectName;
        this.builderService
            .changeProjectName(this.project)
            .subscribe(res => {
                if (res.status === 200) {

                }
                else {
                    console.log(res.json());
                }
            });
    }

    editHtmlSource() {
        this.builderService.sourceHtmlOfFile(this.mainFilePath)
            .subscribe(res => {
                this.htmlSource = res.join("\n");
            });
    }

    closeEditSource() {
        this.htmlSource = null

    }

    saveEditedHtml() {
        this.builderService.saveChangesInEditedSource(this.mainFilePath, this.htmlSource)
            .subscribe(res => {
                if (res.status === 200) {
                    this.closeEditSource();
                }
                else {
                    console.log(res.json());
                }
            });


    }


    ngAfterViewInit() {
        $('#designedPage').load(() => {

            var iframe = $('#designedPage').contents();
            iframe.find("body").click((event: any) => {

                // console.log(event);
                this.openTab('htmlElements', 'tab3');
                this.htmlCollection = this.designedPage.nativeElement.contentDocument.all;
                this.currentCollection = [];
                let localName: string = event.target.localName;
                this.elementName = localName;
                for (var i = 0; i < this.htmlCollection.length; i++) {
                    if (this.htmlCollection[i].localName === localName) {
                        this.currentCollection.push(this.htmlCollection[i]);
                    }
                }
                this.getSelectedItem(event.target.innerHTML);

                //console.log(this.currentCollection);
            });
        });
    }


    ngOnInit() {
        this.openTab('changeTheme', 'tab1');
        this.sub = this.route.params.subscribe(params => {
            this.selectedItemSrc = this.sanitizer.bypassSecurityTrustResourceUrl(params['path']);
            this.mainFilePath = params['path'];
            this.getProjectDetails();
        });
    }
}
