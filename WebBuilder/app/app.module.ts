import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { AppComponent }   from './app.component';
import { DesignerComponent }   from '../app/designer/designer.component';
import { BuilderService }  from './builder.service';
import { TemplatesComponent } from './selectTemplate/selectTemplate.component';
import { HttpModule }    from '@angular/http';
import { AppRoutingModule }     from './app.routing';
import { ColorPickerModule } from 'angular2-color-picker';
import { AuthenticationService } from './authentication.service';

@NgModule({
    imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ColorPickerModule
    ],
    declarations: [
        AppComponent,
        DesignerComponent,
        TemplatesComponent
    ],
    providers: [ BuilderService,AuthenticationService ],
    bootstrap: [ AppComponent ]
})
export class AppModule {}
