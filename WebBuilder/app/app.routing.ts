﻿import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DesignerComponent}   from '../app/designer/designer.component';
import { TemplatesComponent } from './selectTemplate/selectTemplate.component';

const routes: Routes = [
   // {
     //   path: '', 
     //   redirectTo: '/templates',
     //   pathMatch: 'full'
   // },
    {
        path:  '', 
        component: TemplatesComponent
    },   
    {
        path: 'design/:path',
      // path: '',
        component: DesignerComponent
    }
  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}