﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using WebBuilder.Data;
using WebBuilder.Models;
using WebBuilder.Service;
using WebBuilder.ViewModel;
//using System.IO.Compression;
using System.Linq;
using System.Net.Http.Headers;
using Ionic.Zip;

namespace WebBuilder.Controllers
{

    public class ProjectController : ApiController
    {
        private readonly string _baseUrl =
            ConfigurationManager.AppSettings["basePath"];
        private readonly UnitOfWork _unitOfWork;
        public ProjectController()
        {
            _unitOfWork = new UnitOfWork();
        }

        public IEnumerable<Project> GetAllUserProject()
        {
            return _unitOfWork.ProjectRepository
                .GetAllUserProjects(User.Identity.GetUserId<string>());
        }

        [HttpPost]
        public IHttpActionResult ProjectDetails(FileViewModel model)
        {
            var projectDetails = _unitOfWork.ProjectRepository.GetProjectByFilePath(model.MainFilePath);
            if (projectDetails == null)
            {
                return NotFound();
            }
            return Ok(projectDetails);
        }

        [HttpPost]
        public IHttpActionResult ChangeProjectName(Project model)
        {

            var result = _unitOfWork.ProjectRepository.Update(model);
            _unitOfWork.Save();
            if (result)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpDelete]
        public IHttpActionResult DeleteProject(int Id)
        {

            var result = _unitOfWork.ProjectRepository.Remove(Id);
            _unitOfWork.Save();
            if (result)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpPost]
        public HttpResponseMessage DownloadProject(FileViewModel model)
        {
            string path = model.MainFilePath
                .Remove(model.MainFilePath.LastIndexOf('/'));

            using (var zipFile = new ZipFile())
            {
                zipFile.AddDirectory(_baseUrl + path);
                return ZipContentResult(zipFile);
            }

        }

        protected HttpResponseMessage ZipContentResult(ZipFile zipFile)
        {
            var pushStreamContent = new PushStreamContent((stream, content, context) =>
            {
                zipFile.Save(stream);
                stream.Close();
            }, "application/zip");

            return new HttpResponseMessage(HttpStatusCode.OK)
            { Content = pushStreamContent };
        }

    }
}
