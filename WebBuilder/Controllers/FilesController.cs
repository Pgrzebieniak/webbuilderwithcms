﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebBuilder.Data;
using WebBuilder.Models;
using WebBuilder.Service;
using WebBuilder.ViewModel;

namespace WebBuilder.Controllers
{
    public class FilesController : ApiController
    {
        private readonly string _baseUrl =
            ConfigurationManager.AppSettings["basePath"];
        private string _path;
        private string _oldImgSrc;
        private string _newImgSrc;
        private readonly FilesService _filesService;
        private readonly UnitOfWork _unitOfWork;

        public FilesController()
        {
            _unitOfWork = new UnitOfWork();
            _filesService = new FilesService();
        }

        [HttpPost]
        public IHttpActionResult GetProjectFiles(ProjectViewModel model)
        {
            var projectFiles = _unitOfWork.FilesRepository.Get(model.ProjectId);
            if (projectFiles == null)
            {
                return NotFound();
            }

            return Ok(projectFiles);
        }

        [HttpPost]
        public IHttpActionResult SaveChanges(HtmlElement model)
        {
            var result = _filesService.SaveChangesHtml(model);
            if (result == false)
            {
                return BadRequest();
            }

            return Ok();

        }

        [HttpPost]
        public IHttpActionResult SaveChangesInEditetSource(FileViewModel fileViewModel)
        {
            var result = _filesService.SaveEditedHtmlSource(fileViewModel.MainFilePath, fileViewModel.HtmlEditedSource);
            if (result == false)
            {
                return BadRequest();
            }

            return Ok();

        }

        [HttpPost]
        public IHttpActionResult SourceFileHtml(FileViewModel fileViewModel)
        {
            return Ok(_filesService.GetHtmlSource(fileViewModel.MainFilePath));
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Upload()
        {

            if (Request.Content.IsMimeMultipartContent())
            {
                var streamProvider = new MultipartFormDataStreamProvider(Path.Combine(_baseUrl, "Upload"));
                await Request.Content.ReadAsMultipartAsync(streamProvider);

                foreach (var key in streamProvider.FormData.AllKeys)
                {
                    foreach (var value in streamProvider.FormData.GetValues(key))
                    {
                        if (key == "path")
                        {
                            _path = value;
                        }
                        else
                        {
                            _oldImgSrc = value;
                        }

                    }
                }

                foreach (MultipartFileData fileData in streamProvider.FileData)
                {
                    if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                    {
                        return Request.CreateResponse(HttpStatusCode.NotAcceptable,
                            "This request is not properly formatted");
                    }
                    string fileName = fileData.Headers.ContentDisposition.FileName;

                    if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                    {
                        fileName = fileName.Trim('"');
                    }
                    if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                    {
                        fileName = Path.GetFileName(fileName);
                    }

                    File.Move(fileData.LocalFileName,
                        Path.Combine(_baseUrl + _path
                        .Remove(_path.LastIndexOf('/')) + @"\img", fileName));
                    _newImgSrc = @"img\" + fileName;

                    _filesService.SaveNewImage("img", _oldImgSrc, _newImgSrc, _path);
                }
                return Request.CreateResponse(_newImgSrc);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable,
                    "This request is not properly formatted");
            }
        }

    }
}
