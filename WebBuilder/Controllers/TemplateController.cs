﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataHandler.Encoder;
using WebBuilder.Data;
using WebBuilder.Model.Models;
using WebBuilder.Models;
using WebBuilder.Service;
using WebBuilder.ViewModel;

namespace WebBuilder.Controllers
{
   
    public class TemplateController : ApiController
    {
        private readonly TemplateService _templateService;
        private readonly UnitOfWork _unitOfWork;
        private readonly string  _baseUrl=
            ConfigurationManager.AppSettings["basePath"];
        public TemplateController()
        {
            _unitOfWork = new UnitOfWork();
            _templateService=new TemplateService();
        }

        [AllowAnonymous]
        public IEnumerable<Template> GetAllTemplates()
        {
            return _unitOfWork.TemplateRepository.GetAll();
        }
        [HttpPost]
        public string CreateTemplateForUser(TemplateViewModel model)
        {      
            string loggedUser = System.Web.HttpContext.Current.User.Identity.Name;

            var uniqueFileName = string.Format(@"{0}", Guid.NewGuid());
            string sourceDirectory = _baseUrl +
                model.SelectedItemSrc
                .Remove(model.SelectedItemSrc.LastIndexOf('\\'));

            string userTemplatePath = @"app/UserProject/" + uniqueFileName + loggedUser;
            string targetDirectory = _baseUrl + userTemplatePath;
            string mainFilePath = userTemplatePath + "/index.html";

            _templateService.Copy(sourceDirectory, targetDirectory);

            var project = new Project()
            {
                
                UserId = User.Identity.GetUserId<string>(),
                TemplateId =  _unitOfWork.TemplateRepository
                .GetTemplateByPath(model.SelectedItemSrc),
                ProjectName = uniqueFileName
            };
            _unitOfWork.ProjectRepository.Add(project);
            _unitOfWork.Save();

            var files = new Files()
            {
                MainFilePath = mainFilePath,
                ProjectPath = userTemplatePath,
                ProjectId = _unitOfWork.ProjectRepository.GetProjectId(project.ProjectName)
            };
             _unitOfWork.FilesRepository.Add(files);
            _unitOfWork.Save();

            return mainFilePath;
        }

    }
}
