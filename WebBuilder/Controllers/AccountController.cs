﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WebBuilder.Models;
using WebBuilder.ViewModel;
using Microsoft.Owin.Security;


namespace WebBuilder.Controllers
{
    [System.Web.Http.AllowAnonymous]
    public class AccountController : ApiController
    {
        private readonly UserManager<User> _userManager;

        public AccountController()
        : this(Startup.UserManagerFactory.Invoke())
        {
        }

        public AccountController(UserManager<User> userManager)
        {
            this._userManager = userManager;
        }

        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> Login(ViewModelUser model)
        {
            var user = await _userManager
                .FindAsync(model.Email, model.Password);

            if (user != null)
            {
                await SignIn(user);
                return Ok(user.Email);
            }

              return  BadRequest( "Błędny Login lub hasło");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
            }
            base.Dispose(disposing);
        }
        private async Task SignIn(User user)
        {
            var identity = await _userManager
                .CreateIdentityAsync(user,
                DefaultAuthenticationTypes.ApplicationCookie);

            GetAuthenticationManager().SignIn(identity);
        }
        private IAuthenticationManager GetAuthenticationManager()
        {
            var ctx = Request.GetOwinContext();
            return ctx.Authentication;
        }

        public IHttpActionResult LogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return Ok();
        }

        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Błędne dane");
                return BadRequest(ModelState);
            }

            var user = new User
            {
                Email = model.Email,
                UserName = model.Email,
                Name = model.Name,
                LastName = model.LastName
            };

            var result = await _userManager
                .CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await SignIn(user);
                return Ok(user.Email);
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
                return BadRequest(ModelState);
            }

        }
        private string GetLoggedUser()
        {
            return System.Web.HttpContext.Current.User.Identity.Name;
        }
    }
}