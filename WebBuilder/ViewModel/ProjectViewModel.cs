﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBuilder.ViewModel
{
    public class ProjectViewModel
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }

        public string UserId { get; set; }
        public int TemplateId { get; set; }
    }
}