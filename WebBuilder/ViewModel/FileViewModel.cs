﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBuilder.ViewModel
{
    public class FileViewModel
    {
        public int ProjectFilesId { get; set; }
        public string ProjectPath { get; set; }
        public string MainFilePath { get; set; }

        public string HtmlEditedSource { get; set; }

        public int ProjectId { get; set; }
    }
}