﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBuilder.Models
{
    public class LoginSucess
    {

        public Guid SessionId { get; set; }

        public LoginSucess(Guid newGuid)
        {
            SessionId = newGuid;
        }
    }
}