﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using Autofac;
using Autofac.Integration.Mvc;
using WebBuilder.App_Start;
using WebBuilder.Data.Interfaces;
using WebBuilder.Data.Repositories;

namespace WebBuilder
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            var builder = new ContainerBuilder();

            // Register the controller in scope 
            builder.RegisterControllers(typeof(Global).Assembly);

            // Register types
            //builder.RegisterType<FilesRepository>().As<IFilesRepository>();
            //builder.RegisterType<ProjectRepository>().As<IProjectRepository>();
            //builder.RegisterType<UserRepository>().As<IUserRepository>();

            // Build the container
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}