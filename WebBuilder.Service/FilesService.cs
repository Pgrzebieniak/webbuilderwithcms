﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Configuration;
using System.Data.Common;
using System.IO;

namespace WebBuilder.Service
{

    public class FilesService
    {
        private readonly string _url =
               ConfigurationManager.AppSettings["url"];

        private readonly string _basePath =
            ConfigurationManager.AppSettings["basePath"];

        private string _style;


        public bool SaveChangesHtml(HtmlElement model)
        {
            try
            {
                var index = _url.Length - 1;
                var path = model.Path.Remove(0, index);

                var html = new HtmlDocument();

                html.Load(_basePath + path, Encoding.UTF8);
                var root = html.DocumentNode;
                var nodes = root.Descendants();

                HtmlNode node =
                    nodes.Where(x => x.Name == model.Element &&
                                     x.InnerHtml == model.CurrentText)
                        .Select(x => x).SingleOrDefault();

                node.InnerHtml = model.NewText;

                _style = "color:" + model.Color + ";" +
                         "font-size:" + model.FontSize + ";" +
                         "margin-left:" + model.MarginLeft + ";" +
                         "margin-right:" + model.MarginRight + ";" +
                         "margin-top:" + model.MarginTop + ";" +
                         "margin-bottom:" + model.MarginBottom + ";";

                node.SetAttributeValue("style", _style);

                html.Save(_basePath + path);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void SaveNewImage(string element, string oldImgPath,
            string newImgPath, string path)
        {
            var html = new HtmlDocument();

            html.Load(_basePath + path, Encoding.UTF8);
            var root = html.DocumentNode;
            var nodes = root.Descendants();

            HtmlNode node =
                    nodes.Where(x => x.Name == element && x.OuterHtml == oldImgPath)
                    .Select(x => x).SingleOrDefault();

            node.SetAttributeValue("src", newImgPath);

            html.Save(_basePath + path);
        }

        public string[] GetHtmlSource(string path)
        {
            return File.ReadAllLines(_basePath + path);
        }

        public bool SaveEditedHtmlSource(string path, string source)
        {
            try
            {
                File.WriteAllText(_basePath + path, source);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}