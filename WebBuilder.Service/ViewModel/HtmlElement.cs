﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBuilder.Service
{
    public class HtmlElement
    {
        public string Element { get; set; }
        public string CurrentText { get; set; }
        public string NewText { get; set; }
        public string Color { get; set; }
        public string FontSize { get; set; }
        public string MarginLeft { get; set; }
        public string MarginRight { get; set; }
        public string MarginTop { get; set; }
        public string MarginBottom { get; set; }
        public string Path { get; set; }
    }
}
